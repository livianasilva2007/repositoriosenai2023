const {createApp} = Vue;

createApp({
    data(){
        return{
            display:"0",
            operador:null,
            numeroAtual: null,
            numeroAnterior: null,
        };
    },//Fechamento data

    methods:{
        numero(valor){            
            if(this.display == "0"){
                this.display = valor.toString();
            }
            else{
               // this.display = this.display + valor.toString();
                this.display+= valor.toString();

                //Fórmula resumida:
            }
        },//Fechamento numero
        
        decimal(){
            if(!this.display.includes(".")){
               //s this.display = this.display + ".";

                //Fórmula resumida:
                this.display +=".";
            }
        },//Fechamento 

        clear(){
            this.display = "0";
            this.numeroAtual = null;
            this.numeroAnterior = null;
            this.operador = null;
        },//Fechamento clear

        operacoes(operacao){
            if(this.operador != null){
                const displayAtual = parseFloat(this.display);
                switch(this.operador){
                    case "+":
                        this.display = (this.numeroAtual + displayAtual).toString();
                        break;
                    case"-":
                        this.display = (this.numeroAtual - displayAtual).toString();
                        break;
                    case "*":
                        this.display =(this.numeroAtual * displayAtual).toString();
                        break;
                    case"/":
                        this.display = (this.numeroAtual / displayAtual).toString();


                }//Fim do switch
                this.numeroAnterior = this.numeroAtual;
                this.numeroAtual = null;
            }//Fim do if

            if(operacao != "="){

                 this.operador = operacao;
                 this.numeroAtual = parseFloat(this.display);
                 this.display = "0";
            }
        }
     } //Fechamento methods

}).mount("#app");//Fechamento createApp