const { createApp } = Vue; //importa a função createApp do Vue.js.
createApp({
  /*cria e monta a instância Vue.js com as opções passadas como argumento e a vincula ao elemento HTML com o ID "app".*/
  /*A opção passada para createApp() é um objeto com as seguintes propriedades:*/

  data() {
    /*função que retorna um objeto com as propriedades usadas pela aplicação. */

    return {
        paginaInicial:'index.html',
    }; //Fim return
  }, //Fim data

  methods: {
    /*objeto com as funções usadas para manipular o estado da aplicação*/

    loadPage: function () {
      /*Função para carregar a página desejada*/      

      /*comando utilizando para localizar a página que deverá ser carregada*/
      window.location.href = 'pagina2.html';
      
    },//Fim loadPage
  }, //fim methods
}).mount("#app"); //Monte o componente na div com id "app"